# Internet sharing

Start and stop scripts for internet sharing.

## Note of warning

- Internet interface names are still hardcoded
- Configuration iles contain some cruft from real-life work

## Contributors

- Koen van Ingen (initiator)
- Nico Rikken

## License

[MIT license](https://opensource.org/licenses/MIT).
